# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    "name" : "GP_Sketch",
    "author" : "Tom VIGUIER",
    "description" : "Sketching, inking, on a single layer with this TVPaint inspired panel",
    "blender" : (2, 93, 1),
    "version" : (0, 0, 1),
    "location" : "Tool section of 3d view's N-panel",
    "warning" : "",
    "category" : "Andarta"
}
import bpy
from bpy.types import PropertyGroup
from bpy.props import *
from mathutils import Vector


def count_brushes() : 
    count = 0
    for br in bpy.data.brushes :
        if br.name.startswith('_sketch') :
            count += 1 
    return count

def add_color(color):
    
    colornumber = str(count_brushes() + 1)
    bpy.ops.wm.tool_set_by_id(name = "builtin_brush.Draw")
    wm = bpy.context.window_manager
    colors = wm.gpsk_colors
    new_color = colors.add()
    #assign a material
    mat = bpy.data.materials.new("_sketch-" + colornumber)
    new_color.material = mat
    bpy.data.materials.create_gpencil_data(mat)
    mat.grease_pencil.color = color
    #assing a brush
    brush_settings = {'active_smooth_factor': 0.3499999940395355, 'aspect': Vector((1.0, 1.0)), 'fill_factor': 1.0, 'hardness': 1.0, 'input_samples': 10, 'pen_subdivision_steps': 1, 'simplify_factor': 0.0020000000949949026, 'use_pressure': True, 'use_material_pin': True, 'use_settings_postprocess': True, 'vertex_color_factor': 1.0, 'vertex_mode': 'BOTH'}
    




    brushes = []
    for brush in bpy.data.brushes:
        brushes.append(brush.name)

    bpy.ops.brush.add_gpencil()

    for brush in bpy.data.brushes:
        if brush.name not in brushes:
            br = brush
            break

    br.name = '_sketch-' + colornumber 

    for attr in brush_settings:
        setattr(br.gpencil_settings, attr, brush_settings[attr])
    br.gpencil_settings.material = new_color.material
    new_color.brush = br
    return new_color

class GPSK_COLOR(PropertyGroup) :
    bl_label = "sketch color"
    bl_idname = "gpsk_color"
    material : PointerProperty(type = bpy.types.Material)
    brush : PointerProperty(type = bpy.types.Brush)



class GPSK_PT_PANEL(bpy.types.Panel):
   
    bl_label = "GP Sketch addon"
    bl_idname = "GPSK_PT_PANEL"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Tool"

    def draw(self, context):
        layout = self.layout
        #layout.label(text='test')
        
        if context.mode == 'PAINT_GPENCIL':
            if len(bpy.context.window_manager.gpsk_colors) == 0 :
                layout.operator("gpsk.start", text = 'Start')
            else :
                box = layout.box()
                for i, color in enumerate(context.window_manager.gpsk_colors):
                    if color.material and color.material.is_grease_pencil:
                
                        row = box.row(align = True)
  
                        row.prop(color.material.grease_pencil, "color", text = '')

                        pencil = row.operator("gpsk.pencil", text = ' ', icon = 'GREASEPENCIL', depress = color.brush == context.scene.tool_settings.gpencil_paint.brush )
                        pencil.color_index = i
                        
                        repaint = row.operator("gpsk.repaint", text = '', icon = 'MATFLUID')
                        repaint.color_index = i

                        clear_others = row.operator("gpsk.clear_others", text = '', icon = 'DRIVER_TRANSFORM')
                        clear_others.color_index = i

                        clear = row.operator("gpsk.clear", text = '', icon = 'X')
                        clear.color_index = i
                        
                        row.prop(color.material.grease_pencil, "hide", text = '')
                        row.prop(color.material.grease_pencil, "lock", text = '')

                        if color.material.grease_pencil.ghost == True :
                            OSicon = 'ONIONSKIN_OFF'
                        else :
                            OSicon = 'ONIONSKIN_ON'

                        row.prop(color.material.grease_pencil, "ghost", text = '', icon = OSicon)

                        remove = row.operator("gpsk.del", text = '', icon = 'TRASH')
                        remove.color_index = i


                layout.operator("gpsk.add", text = 'Add')

class GPSK_OT_TEST(bpy.types.Operator):
    bl_label = "GP Sketch test op (for layout purpose)"
    bl_idname = "gpsk.test"
    bl_options = {'REGISTER','UNDO'}  
    def execute(self,context) :
        print('test')
        #new_gpsk_color((1.0, 0.0, 0.0, 1.0))
        return {'FINISHED'}

class GPSK_OT_REPAINT(bpy.types.Operator):
    bl_label = "Repaint sketch strokes"
    bl_idname = "gpsk.repaint"
    bl_options = {'UNDO'}  
    color_index : IntProperty()
    def execute(self,context) :
        color = context.window_manager.gpsk_colors[self.color_index]
        ob = context.object
        slot_index = None
        for i, material in enumerate(ob.data.materials):
            if material == color.material:
                slot_index = i
                break
        if not slot_index:
            ob.data.materials.append(color.material)
            slot_index = len(ob.data.materials) - 1
        layer = ob.data.layers.active
        if layer.lock == False:

            frames = [layer.active_frame]
            if ob.data.use_multiedit :
                for frame in layer.frames :
                    if frame.select and frame not in frames:
                        frames.append(frame)

            for frame in frames:                        
                for stroke in frame.strokes: 
                    if ob.material_slots[stroke.material_index].material.grease_pencil.lock == False:
                        stroke.material_index = slot_index
                
        return {'FINISHED'}

class GPSK_OT_CLEAR(bpy.types.Operator):
    bl_label = "Clear strokes using this color"
    bl_idname = "gpsk.clear"
    bl_options = {'UNDO'}  
    color_index : IntProperty()
    def execute(self,context) :
        color = context.window_manager.gpsk_colors[self.color_index]
        ob = context.object

        if color.material.grease_pencil.lock == False :             

            slot_index = None
            for i, material in enumerate(ob.data.materials):
                if material == color.material:
                    slot_index = i
                    break
            
            layer = ob.data.layers.active
            if layer.lock == False:

                frames = [layer.active_frame]
                if ob.data.use_multiedit :
                    for frame in layer.frames :
                        if frame.select and frame not in frames:
                            frames.append(frame)

                for frame in frames:
                    del_strokes = []
                    for stroke in frame.strokes: 
                        if stroke.material_index == slot_index:
                            del_strokes.append(stroke)
                    for stroke in del_strokes:
                        frame.strokes.remove(stroke)
        
        return {'FINISHED'}

class GPSK_OT_CLEAROTHERS(bpy.types.Operator):
    bl_label = "Clear frame except strokes using this color"
    bl_idname = "gpsk.clear_others"
    bl_options = {'UNDO'}  
    color_index : IntProperty()
    def execute(self,context) :
        color = context.window_manager.gpsk_colors[self.color_index]
        ob = context.object
        slot_index = None
        for i, material in enumerate(ob.data.materials):
            if material == color.material:
                slot_index = i
                break
        
        layer = ob.data.layers.active
        if layer.lock == False:

            frames = [layer.active_frame]
            if ob.data.use_multiedit :
                for frame in layer.frames :
                    if frame.select and frame not in frames:
                        frames.append(frame)

            for frame in frames:
                del_strokes = []
                for stroke in frame.strokes: 
                    if stroke.material_index != slot_index and ob.material_slots[stroke.material_index].material.grease_pencil.lock == False :
                        del_strokes.append(stroke)
                for stroke in del_strokes:
                    frame.strokes.remove(stroke)
        return {'FINISHED'}

class GPSK_OT_PENCIL(bpy.types.Operator):
    bl_label = "Draw with sketch color"
    bl_idname = "gpsk.pencil"
    bl_options = {'REGISTER', 'UNDO'}  
    color_index : IntProperty()
    def execute(self,context) :
        color = context.window_manager.gpsk_colors[self.color_index]
        context.scene.tool_settings.gpencil_paint.brush = color.brush

        return {'FINISHED'}
        
class GPSK_OT_DEL(bpy.types.Operator):
    bl_label = "Remove sketch color"
    bl_idname = "gpsk.del"
    bl_options = {'REGISTER','UNDO'}  
    color_index : IntProperty()
    def execute(self,context) :
        color = context.window_manager.gpsk_colors[self.color_index]
        
        #if you're deleting the active brush: pick another brush before removing the selected one
        if color.brush == context.scene.tool_settings.gpencil_paint.brush :  
            for other_brush in bpy.data.brushes :
                if other_brush.use_paint_grease_pencil and other_brush != color.brush :
                    bpy.context.scene.tool_settings.gpencil_paint.brush = other_brush
                    break
        
        #remove brush
        bpy.data.brushes.remove(color.brush)

        #clean materials
        if color.material.users <= 1:
            bpy.data.materials.remove(color.material)
            print('del material')
        else:
            color.material.name = color.material.name[1:]
            print('rename material')
        
        #remove color
        context.window_manager.gpsk_colors.remove(self.color_index)
        return {'FINISHED'}

class GPSK_OT_ADD(bpy.types.Operator):
    bl_label = "add a new sketch color"
    bl_idname = "gpsk.add"
    bl_options = {'REGISTER','UNDO'}  
    color : FloatVectorProperty(size = 4, default = (0.0, 0.0, 0.0, 1.0))
    def execute(self, context) :
        add_color(self.color)
        
        return {'FINISHED'}

class GPSK_OT_START(bpy.types.Operator):
    bl_label = "start sketch panel and create default colors"
    bl_idname = "gpsk.start"
    bl_options = {'REGISTER','UNDO'}  
    color : FloatVectorProperty(size = 4, default = (0.0, 0.0, 0.0, 1.0))

    def execute(self, context) :
        wm = bpy.context.window_manager
        colors = wm.gpsk_colors
        if count_brushes() == 0 : #FIRST START
            default_colors = [(0.0, 0.0, 0.0, 1.0), (1.0, 0.0, 0.0, 1.0), (0.0, 1.0, 0.0, 1.0), (0.0, 0.0, 1.0, 1.0)] 
            for color in default_colors:
                add_color(color)
        
        else : #RECOVER EXISTING COLORS
            for brush in bpy.data.brushes :
                if brush.name.startswith('_sketch'):
                    new_color = colors.add()
                    new_color.brush = brush
                    new_color.material = brush.gpencil_settings.material

        return {'FINISHED'}



#-------REGISTERING ------          
classes = [ GPSK_PT_PANEL, GPSK_OT_TEST, GPSK_COLOR, GPSK_OT_ADD, GPSK_OT_START, GPSK_OT_DEL, GPSK_OT_PENCIL, GPSK_OT_CLEAR, GPSK_OT_REPAINT, GPSK_OT_CLEAROTHERS
    ]               
                      
def register():
    for cls in classes :
        bpy.utils.register_class(cls)
    bpy.types.WindowManager.gpsk_colors = CollectionProperty(type = GPSK_COLOR)
    
    
    
    

def unregister():
    for cls in reversed(classes) :
        bpy.utils.unregister_class(cls)
    del bpy.types.WindowManager.gpsk_colors
    
     
if __name__ == "__main__":
    register()
