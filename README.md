# GP_Sketch

TVPaint inspired sketch Panel

# [Download latest as zip](https://gitlab.com/andarta-pictures/gp-sketch/-/archive/master/gp-sketch-master.zip)

# Installation

Download the repository as a .zip file, access it with blender through Edit/Preferences/Add-ons/Install, then activate it by checking the box in the add-on list.

# User guide

- Find the add-on tab in the tool section of the N-Panel (3D View) and click start.

![guide](docs/gpsk_01.png "guide")

- Use the following operators to draw several passes (from rough to clean) on a single layer.

![guide](docs/gpsk_02.png "guide")

- When creating a new sketch brush/color, the add-on will use the current brush settings (if none selected it will use 'Pen')

# Warnings

This tool is in development stage, thank you for giving it a try ! please report any bug or suggestion to t.viguier@andarta-pictures.com

# Visit us 

[https://www.andarta-pictures.com/](https://www.andarta-pictures.com/)


